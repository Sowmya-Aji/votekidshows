# Overview

Django project to demonstrate test driven development using REST FRAMEWORK and Django's built in unit tests applied on a Postgresql database

## Installation

This project requires python 3.6.8 [https://www.python.org/downloads/mac-osx/] and postgresql [https://www.postgresql.org/download/macosx/]

If you don't have them, please install them from the above links.  Once they are installed, type on the command line:

``` 
$ git clone https://gitlab.com/Sowmya-Aji/votekidshows.git
$ pipenv shell
$ pip install -r requirements.txt
$ cd kidshows
$ createdb kid_shows_drf
$ python manage.py makemigrations
$ python manage.py migrate

```

## Testing


Make sure the directory is votekidshows/kidshow

The command to check the directory path is:

```
$ pwd
```

To run the tests from the command line:

``` 
$ python manage.py test

```

## Output

```
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
..........
----------------------------------------------------------------------
Ran 10 tests in 0.241s

OK

```

