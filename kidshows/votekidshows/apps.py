from django.apps import AppConfig


class VotekidshowsConfig(AppConfig):
    name = 'votekidshows'
