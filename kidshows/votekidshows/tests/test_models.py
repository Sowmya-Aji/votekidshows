from django.test import TestCase
from ..models import Show


class ShowTest(TestCase):
    """Test module for Show model"""

    def setUp(self):
        Show.objects.create(name="Casper", director="Casp",
                            main_character="Casper", channel="CaspTV")

    def test_get_show(self):
        show_casper = Show.objects.get(name="Casper")
        self.assertEqual(
            show_casper.get_show(), 'Casper is directed by Casp on CaspTV starring Casper'
        )
